using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ServiceReference;

namespace RadissonVenueClient
{
    [ApiController]
    [Route("[controller]")]
    public class BookingController : Controller
    {
        [HttpGet]
        public IActionResult Get(){
            return Ok("Booking API is running");
        }

        [HttpPost]
        public async Task<IActionResult> Post(BookingModel bookingData){
            var client = new VenueServiceClient();
            var sendeObject = new ServiceReference.BookingModel();

            System.Console.WriteLine("Booking controller received: " + JsonConvert.SerializeObject(bookingData));

            sendeObject.address = bookingData.address;
            sendeObject.enddate = bookingData.enddate;
            sendeObject.startdate = bookingData.startdate;
            sendeObject.numberofpersons = bookingData.numberofpersons;
            sendeObject.eventid = bookingData.eventid;
            sendeObject.request_type = bookingData.request_type;

            var response = await client.RequestBookingAsync(sendeObject);
            var respbody = response.Body.RequestBookingResult;
            var BRM = new BookingResModel();
            BRM.description = respbody.description;
            BRM.additional_info=respbody.additinal_info;
            BRM.status = respbody.status;
            BRM.eventid = respbody.eventid;
            BRM.request_type = respbody.request_type;

            System.Console.WriteLine("Booking Controller sending: " + JsonConvert.SerializeObject(BRM));

            return Json(BRM);

        }
    }
}